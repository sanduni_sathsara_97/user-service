package com.UserService.UserService.repositories;

import com.UserService.UserService.enities.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Long> {

    @Query("SELECT ue FROM UserEntity ue WHERE ue.id = ?1")
    UserEntity findUserEntityBy(Long id);

    @Query("SELECT ue FROM UserEntity ue WHERE ue.id =?1 AND ue.name=?2")
    UserEntity findUserEntityBy(long id,String name);
}

