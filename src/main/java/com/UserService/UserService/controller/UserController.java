package com.UserService.UserService.controller;

import com.UserService.UserService.services.UserService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.UserService.UserService.dtos.UserDTO;


import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/api/user")
public class UserController {


   private UserService userService;

    //localhost:8082/api/user/getAll
   @GetMapping("/getAll")
   public List<UserDTO> getAllUsers(){
       return userService.getAllUsers();
   }

    //localhost:8082/api/user/getDummyUsers
    @GetMapping("/getDummyUsers")
    public List<UserDTO> getDummyUsers(){
        return Arrays.asList(
                new UserDTO("1","sanduni","21"),
                new UserDTO("2","sathsara","23"),
                new UserDTO("3","Geethika","26"),
                new UserDTO("4","sathsarani","22")
        );
    }
}
