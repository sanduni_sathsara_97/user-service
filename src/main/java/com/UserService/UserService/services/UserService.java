package com.UserService.UserService.services;

import com.UserService.UserService.dtos.UserDTO;
import com.UserService.UserService.repositories.UserRepository;

import java.util.List;
import java.util.stream.Collectors;

public class UserService {

    private UserRepository repository;

    public List<UserDTO> getAllUsers(){
        List<UserDTO> userDTOS= null;
        try{
           userDTOS =repository.findAll()
                   .stream()
                   .map(user -> new UserDTO(
                           user.getId().toString(),
                           user.getName(),
                           user.getAge()
                           )).collect(Collectors.toList());
        }catch (Exception e){

        }
        return userDTOS;
    }

}
